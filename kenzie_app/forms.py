from django import forms


class MakerSelect(forms.Form):
    maker = forms.ChoiceField()


class CarRentalForm(forms.Form):
    make = forms.CharField(label='Vehicle Maker')
    model = forms.CharField(label='Vehicle Model')
    type = forms.CharField(label='Vehicle Type')
    year = forms.IntegerField(label='Vehicle Year')

    city = forms.CharField()
    country = forms.CharField()
    state = forms.CharField()
    latitude = forms.FloatField()
    longitude = forms.FloatField()

    owner = forms.IntegerField(label='Owner ID')
    daily_rate = forms.IntegerField(label='Daily Rate')

    fuelType = forms.CharField(label='Fuel Type')
    rating = forms.FloatField()
    renterTripsTaken = forms.IntegerField(label='Renter Trips Taken')
    reviewCount = forms.IntegerField(label="Review Count")
