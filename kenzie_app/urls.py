from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('cars/', views.list_car_rental, name='list_car_rental'),
    path('cars/new/', views.get_car_rental, name='get_car_rental'),
]
