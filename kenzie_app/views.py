from django.shortcuts import render, redirect
from .models import CarRental, Vehicle, Location, Owner, Rate
from .forms import MakerSelect, CarRentalForm


def home(request):
    return render(request, 'kenzie_app/home.html', {})


def get_car_rental(request):
    if request.method == "POST":
        form = CarRentalForm(request.POST)
        if form.is_valid():
            try:
                vehicle = Vehicle.objects.get(
                                  make=form['make'].value(),
                                  model=form['model'].value(),
                                  type=form['type'].value(),
                                  year=form['year'].value())
            except Exception:
                vehicle = Vehicle.objects.create(
                                  make=form['make'].value(),
                                  model=form['model'].value(),
                                  type=form['type'].value(),
                                  year=form['year'].value())

            try:
                location = Location.objects.get(
                                    city=form['city'].value(),
                                    country=form['country'].value(),
                                    state=form['state'].value(),
                                    latitude=form['latitude'].value(),
                                    longitude=form['longitude'].value())
            except Exception:
                location = Location.objects.create(
                                    city=form['city'].value(),
                                    country=form['country'].value(),
                                    state=form['state'].value(),
                                    latitude=form['latitude'].value(),
                                    longitude=form['longitude'].value())

            try:
                owner = Owner.objects.get(id=form['owner'].value())
            except Exception:
                owner = Owner.objects.create(id=form['owner'].value())

            try:
                rate = Rate.objects.get(daily=form['daily_rate'].value())
            except Exception:
                rate = Rate.objects.create(daily=form['daily_rate'].value())

            CarRental.objects.create(
                            fuelType=form['fuelType'].value(),
                            rating=form['rating'].value(),
                            renterTripsTaken=form['renterTripsTaken'].value(),
                            reviewCount=form['reviewCount'].value(),
                            vehicle=vehicle,
                            location=location,
                            owner=owner,
                            rate=rate)

            return redirect('home')
    else:
        form = CarRentalForm()
    return render(request, 'kenzie_app/get_car_rental.html', {'form': form})


def list_car_rental(request):
    vehicles = Vehicle.objects.values_list('make', flat=True)
    vehicles = sorted(set(vehicles))
    choices = [(vehicle, vehicle) for vehicle in vehicles]

    if request.method == "POST":
        form = MakerSelect(request.POST)
        form.fields['maker'].choices = choices

        vehicles = Vehicle.objects.filter(make=form['maker'].value())
        ids = vehicles.values_list('id', flat=True)
        rentals = CarRental.objects.filter(vehicle__id__in=ids)
        return render(
                      request,
                      'kenzie_app/list_car_rental.html',
                      {'rentals': rentals, 'form': form}
                      )

    else:
        form = MakerSelect()
        form.fields['maker'].choices = choices

        vehicles = Vehicle.objects.filter(make=choices[0][0])
        ids = vehicles.values_list('id', flat=True)
        rentals = CarRental.objects.filter(vehicle__id__in=ids)
        return render(
                      request,
                      'kenzie_app/list_car_rental.html',
                      {'rentals': rentals, 'form': form}
                      )
