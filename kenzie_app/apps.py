from django.apps import AppConfig


class KenzieAppConfig(AppConfig):
    name = 'kenzie_app'
