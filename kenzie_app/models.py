from django.db import models


class Rate(models.Model):
    daily = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return 'Daily rate: %d\n' % (self.daily)


class Owner(models.Model):
    id = models.BigIntegerField(primary_key=True)

    def __str__(self):
        return 'Owner id: %d\n' % (self.id)


class Location(models.Model):
    city = models.TextField()
    country = models.TextField()
    latitude = models.FloatField()
    longitude = models.FloatField()
    state = models.TextField()

    def __str__(self):
        return 'City: %s\nState: %s\nCountry: %s\nLatitude: %s\nLongitude: %s'\
                % (self.city, self.state, self.country, self.latitude,
                   self.longitude)


class Vehicle(models.Model):
    make = models.TextField()
    model = models.TextField()
    type = models.TextField()
    year = models.IntegerField()

    def __str__(self):
        return 'Maker: %s\nModel: %s\nType: %s\nYear: %d' % (self.make,
                                                             self.model,
                                                             self.type,
                                                             self.year)


class CarRental(models.Model):
    fuelType = models.TextField()
    rating = models.FloatField(blank=True, null=True)
    renterTripsTaken = models.IntegerField()
    reviewCount = models.IntegerField()
    owner = models.ForeignKey(Owner, on_delete=models.CASCADE)
    rate = models.ForeignKey(Rate, blank=True, null=True, on_delete=models.CASCADE)
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    vehicle = models.ForeignKey(Vehicle, on_delete=models.CASCADE)

    def __str__(self):
        return 'Fuel type: %s\nRating: %f\nRenter trips taken: %d\nNumber of reviews: %d\n'\
         % (self.fuelType, self.rating, self.renterTripsTaken, self.reviewCount)
