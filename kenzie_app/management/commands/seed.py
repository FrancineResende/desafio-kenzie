from django.core.management.base import BaseCommand
from kenzie_app.models import Rate, Owner, Location, Vehicle, CarRental
import csv


class Command(BaseCommand):

    def handle(self, *args, **options):
        filename = 'CarRentalData.csv'
        print(" # Opening file", filename)
        with open(filename) as file:
            rents = csv.DictReader(file)

            print(" # Parsing and persisting data. This might take a while...")

            i = 0
            for row in rents:
                try:
                    owner = Owner.objects.get(id=row['owner.id'])
                except Exception:
                    owner = Owner.objects.create(id=row['owner.id'])

                try:
                    rate = Rate.objects.get(daily=row['rate.daily'])
                except Exception:
                    rate = Rate.objects.create(daily=row['rate.daily'])

                try:
                    location = Location.objects.get(
                                        latitude=row['location.latitude'],
                                        longitude=row['location.longitude'])
                except Exception:
                    location = Location.objects.create(
                                        city=row['location.city'],
                                        country=row['location.country'],
                                        latitude=row['location.latitude'],
                                        longitude=row['location.longitude'],
                                        state=row['location.state'])

                try:
                    vehicle = Vehicle.objects.get(
                                      make=row['vehicle.make'],
                                      model=row['vehicle.model'],
                                      type=row['vehicle.type'],
                                      year=row['vehicle.year'])
                except Exception:
                    vehicle = Vehicle.objects.create(
                                      make=row['vehicle.make'],
                                      model=row['vehicle.model'],
                                      type=row['vehicle.type'],
                                      year=row['vehicle.year'])

                if row['rating'] == '':
                    row['rating'] = None

                CarRental.objects.create(
                          fuelType=row['fuelType'],
                          rating=row['rating'],
                          renterTripsTaken=row['renterTripsTaken'],
                          reviewCount=row['reviewCount'],
                          owner=owner,
                          rate=rate,
                          location=location,
                          vehicle=vehicle)

                i += 1

            print(" # Inserted %d car rental objects" % i)
