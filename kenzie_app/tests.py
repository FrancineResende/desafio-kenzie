from django.test import TestCase, Client
from django.urls import reverse
from .models import Vehicle, Location, Owner, Rate, CarRental


class VehicleTestClass(TestCase):
    @classmethod
    def setUpTestData(cls):
        Vehicle.objects.create(make="Volkswagen", model="Gol",
                               type="car", year=1996)

    def test_object_representation(self):
        vehicle = Vehicle.objects.get(make="Volkswagen")
        expected_obj = 'Maker: %s\nModel: %s\nType: %s\nYear: %d' % (
            vehicle.make, vehicle.model, vehicle.type, vehicle.year)
        self.assertEquals(expected_obj, str(vehicle))


class LocationTestClass(TestCase):
    @classmethod
    def setUpTestData(cls):
        Location.objects.create(city="Taubaté", state="SP", country="Brasil",
                                latitude=-23.001006, longitude=-45.544795)

    def test_object_representation(self):
        location = Location.objects.get(city="Taubaté")
        expected_obj = 'City: %s\nState: %s\nCountry: %s\nLatitude: %s\nLongitude: %s'\
            % (location.city, location.state, location.country,
               location.latitude, location.longitude)
        self.assertEquals(expected_obj, str(location))


class OwnerTestClass(TestCase):
    @classmethod
    def setUpTestData(cls):
        Owner.objects.create(id=1)

    def test_object_representation(self):
        owner = Owner.objects.get(id=1)
        expected_obj = 'Owner id: %d\n' % (owner.id)
        self.assertEquals(expected_obj, str(owner))


class RateTestClass(TestCase):
    @classmethod
    def setUpTestData(cls):
        Rate.objects.create(daily=50)

    def test_object_representation(self):
        rate = Rate.objects.get(daily=50)
        expected_obj = 'Daily rate: %d\n' % (rate.daily)
        self.assertEquals(expected_obj, str(rate))


class CarRentalTestClass(TestCase):
    @classmethod
    def setUpTestData(cls):
        vehicle = Vehicle.objects.create(make="Volkswagen", model="Gol",
                                         type="car", year=1996)
        location = Location.objects.create(city="Taubaté", state="SP",
                                           country="Brasil",
                                           latitude=-23.001006,
                                           longitude=-45.544795)
        owner = Owner.objects.create(id=1)
        rate = Rate.objects.create(daily=50)

        CarRental.objects.create(
                        fuelType="Gasolina",
                        rating=4.1,
                        renterTripsTaken=30,
                        reviewCount=14,
                        vehicle=vehicle,
                        location=location,
                        owner=owner,
                        rate=rate)

    def test_object_representation(self):
        car = CarRental.objects.get(fuelType="Gasolina")
        expected_obj = 'Fuel type: %s\nRating: %f\nRenter trips taken: %d\nNumber of reviews: %d\n'\
            % (car.fuelType, car.rating, car.renterTripsTaken, car.reviewCount)
        self.assertEquals(expected_obj, str(car))


class ViewsTestClass(TestCase):
    @classmethod
    def setUpTestData(cls):
        vehicle = Vehicle.objects.create(make="Volkswagen", model="Gol",
                                         type="car", year=1996)
        location = Location.objects.create(city="Taubaté", state="SP",
                                           country="Brasil",
                                           latitude=-23.001006,
                                           longitude=-45.544795)
        owner = Owner.objects.create(id=1)
        rate = Rate.objects.create(daily=50)
        CarRental.objects.create(
                        fuelType="Gasolina",
                        rating=4.1,
                        renterTripsTaken=30,
                        reviewCount=14,
                        vehicle=vehicle,
                        location=location,
                        owner=owner,
                        rate=rate)

    def test_home(self):
        url = reverse('home')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'kenzie_app/home.html')

    def test_get_car_rental_form_fields(self):
        response = self.client.get('/cars/new/')

        expected_fields = ['make', 'model', 'type', 'year', 'city', 'country',
                           'state', 'latitude', 'longitude', 'owner',
                           'daily_rate', 'fuelType', 'rating',
                           'renterTripsTaken', 'reviewCount']

        form_fields = [field.name for field in response.context['form']]
        for field in expected_fields:
            if self.assertIn(field, form_fields):
                pass

    def test_get_car_rental_http_get(self):
        url = reverse('get_car_rental')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'kenzie_app/get_car_rental.html')

    def test_get_car_rental_http_post(self):
        url = reverse("get_car_rental")
        client_json = {"make": "Volkswagen", "model": "Gol", "type": "car",
                       "year": 1997, "city": "Taubaté", "state": "SP",
                       "country": "Brasil", "latitude": -23.001006,
                       "longitude": -45.544795, "owner_id": 1234,
                       "daily_rate": 30, "fuelType": "Gasoline", "rating": 4.0,
                       "renterTripsTaken": 28, "reviewCount": 19}

        response = self.client.post(url, client_json)
        self.assertEqual(response.status_code, 200)

    def test_list_car_rental_http_get(self):
        url = reverse('list_car_rental')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'kenzie_app/list_car_rental.html')

    def test_list_car_rental_http_post(self):
        url = reverse('list_car_rental')
        client_json = {"maker": "Volkswagen"}
        response = self.client.post(url, client_json)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(len(response.context["rentals"]) > 0)
