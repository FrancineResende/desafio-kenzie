#!/bin/sh
python manage.py makemigrations
python manage.py migrate
python manage.py seed
python manage.py test
exec "$@"
